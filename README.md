# Coffre Très Fort

Quentin BERNARD & Robin LEFEBVRE

## Description du projet

“Coffre Très Fort” est un site internet qui offre aux utilisateurs la possibilité de stocker leurs mots de passe de manière sécurisée. L'accès au coffre-fort se fait via un compte utilisateur et une double authentification basée exclusivement sur Google Authenticator.

L'objectif principal de ce projet était de garantir un haut niveau de sécurité pour les utilisateurs. Nous avons mis en place plusieurs mesures de sécurité pour atteindre cet objectif. Tout d'abord, le mot de passe utilisé pour se connecter est hashé dans la base de données, ce qui signifie que nous n'avons aucune visibilité sur le mot de passe réel. Cette mesure vise à protéger les informations sensibles des utilisateurs.

De plus, les mots de passe sauvegardés par les utilisateurs sont chiffrés avant d'être stockés dans la base de données. Nous avons utilisé un chiffrement symétrique pour cette opération. La clé de chiffrement est stockée sur nos serveurs, hors de la portée des utilisateurs. Cela nous permet de garantir que nous ne stockons aucun mot de passe en clair dans notre système. Cependant, cette approche soulève une problématique : nous ne sommes pas autorisés à avoir des mots de passe que nous pouvons techniquement lire dans notre base de données. Pour résoudre ce problème, il serait nécessaire de demander l'approbation explicite de l'utilisateur à travers des conditions d'utilisation, même si cela n'a pas été implémenté dans ce projet qui n'a pas vocation à être déployé.

Pour la réalisation de ce projet, nous avons utilisé J2E (Java Enterprise Edition) et Tomcat, qui sont des technologies populaires pour le développement d'applications web sécurisées et fiables.

Vidéo de présentation : lien

## Fonctionnalités

- Création d'un compte utilisateur avec une double authentification basée sur Google Authenticator.
- Connexion sécurisée au coffre-fort de mots de passe en ligne.
- Affichage de la liste des mots de passe sauvegardés par l'utilisateur.
- Hachage du mot de passe de connexion dans la base de données pour une protection renforcée.
- Chiffrement des mots de passe sauvegardés par l'utilisateur avant de les stocker dans la base de données.
- Utilisation d'un chiffrement symétrique pour le stockage sécurisé des mots de passe.
- Clé de chiffrement inaccessible aux utilisateurs, garantissant que les mots de passe sont stockés de manière sécurisée.
- Demande de validation des conditions d'utilisation pour contourner les restrictions de stockage de mots de passe lisibles techniquement.
- Utilisation de J2E et Tomcat pour le développement du projet.
- Sécurisation de la page de la liste des mots de passe, empêchant l'accès sans connexion.
- Prévention des injections SQL pour renforcer la sécurité.
- Réinitialisation de la session après 5 minutes d'inactivité pour une protection supplémentaire

## Mon rôle

Sur ce projet, je me suis occupé de la mise en place de la base de données, de la connection d'un utilisateur, du stockage de MDP et de la double authentification grâce à une librairie faite pour.

## Mode d’emploie

Afin d’utiliser le projet, il faut un serveur Tomcat. Désormais . désignera le dossier tomcat.
Il faut déplacer le dossier coffre-fort dans le dossier : ./webapps

Après, il faut également mettre la totalité du contenu du dossier ./webapps/coffre-fort/WEB-INF/lib/others dans le dossier ./lib, il faut également ajouter le driver de votre base de données.

Puis, afin de compiler tous les fichiers, il faut exécuter cette commande dans le répertoire ./webapps/coffre-fort/WEB-INF: 
```javac -cp "lib/servlet-api.jar;lib/others/googleauth-1.4.0.jar;lib/junit/junit-4.13.2.jar;lib/junit/junit-jupiter-api-5.7.0.jar;lib/junit/apiguardian-api-1.1.0.jar" ./classes//.java``` (Commande à adapter en fonction du système d’exploitation, les “;” en “:” sur Linux par exemple). 

Attention les .class sont bien à laisser dans le même répertoire que les .java . 

Il faut ensuite avoir une base de données qui fonctionne. Pour ce faire, il faut installer la base donnée (postgresql est mieux, car le driver y est déjà, si une autre base de données, il faut ajouter le driver dans ./webapps/coffre-fort/WEB-INF/lib) et modifier le document ./webapps/coffre-fort/WEB-INF/classes/sql/login.properties. Il faut modifier les lignes url, nom et mdp.

Pour finir avec la base de données, il faut lancer les tests du programme dans cet ordre : TestUserBdd > TestCoffreFortBdd
Afin d’avoir les tables de créer avec un utilisateur test avec ces identifiants
email : robin@robin.fr
mot de passe : bidule (plutôt mauvais mot de passe)

Si le serveur est lancé depuis l’iut aucune modification n’est à apporter.

Il faut maintenant lancer le serveur tomcat, pour ce faire, il faut aller dans le répertoire ./bin , une fois dedans il faut exécuter la commande : Unix : ./catalina.sh run Windows : catalina.bat run

Il ne reste plus qu’à aller sur le site pour ce faire, il suffit d’aller sur ce lien : localhost:8080/coffre-fort/

