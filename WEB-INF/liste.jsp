<!DOCTYPE html>
<%@page import="sql_coffre_fort.Cryptage"%>
<%@page import="sql_coffre_fort.CoffreFort"%>
<%@page import="sql_coffre_fort.CoffreFortDaoJdbc"%>
<html>
 <head>
 <%@ page pageEncoding="UTF-8" %>
   <link rel="stylesheet" href="./css/styles.css">
 <title> MaPage </title>
 </head>
 <body>
 <center>
 <h1>Vos mots de passe</h1>
 <div class="form-add-passwd">
  	<form action="./Add" method="POST" class="auth">
 		<label> Mot de passe <input type="text" name="password" required></label>
 		<label> Description <input type="text" name="desc" required></label>
 		<input class="index-button" type="submit" value="Ajouter un mot de passe" required>
 	</form>
</div>
<a href="./Controller?action=login"  class="index-button">Déconnexion</a>
 <div>
 <%
session = request.getSession( true );
if(session==null || session.getAttribute("id")==null || (session.getAttribute("id")!=null && (int)(session.getAttribute("id"))==-1)){
	response.sendRedirect("./Controller?action=login");
}else{
	CoffreFortDaoJdbc cf = new CoffreFortDaoJdbc();
	CoffreFort r = cf.getCoffreFortWithId((int)(session.getAttribute("id")));
	for(int i = 0; i<r.getListeMdp().size(); i++){
		out.println("<span>");
    out.println("<h4>N°"+(i+1)+"</h4>"+
    "<form action=\"./Delete\" method=\"POST\"><input type=\"text\" value=\""+ r.getListeMdp().get(i).getId_mdp() +"\" name=\"id_pwd\" style=\"display:none;\"> <input type=\"submit\" value=\"X\" required></form>");
		out.println("Mot de passe<br>" + Cryptage.toUncrypt(Cryptage.stringToHex( r.getListeMdp().get(i).getMdp())) );
		out.println("<br><br>Description : " + r.getListeMdp().get(i).getDescription());
		out.println("<br><br></span>");
	}
}
 %>


 </div>
 </center>
 </body>
</html>
